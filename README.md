# Leenucs

My linux stuff: Bash and Python scripts and tools, dotfiles & more

I tested (end I use this everyday) on my Fedora Thinkpad T430, with Plasma as DE and i3-gaps as WM.



## Scripts

See below for crontab


### Backup

**Path:** `/scripts/backup`

**Requirements:** [telegram-send (python)](https://pypi.org/project/telegram-send/)

A bash script that does the backup of 2 folders on a remote server using rsync. It uses telegram-send to notify the user.

Better to use with cron.


### Battery

**Path:** `/scripts/battery.sh`

**Requirements**: [Tuned](https://tuned-project.org/), [Tuned-adm](https://linux.die.net/man/1/tuned-adm)

A bash script that checks if laptop is on AC. If unplugged, sets the tuned profile `powersave`. Else, `balanced`.

This is pretty useless unless you set the execution every minute with cron


### C Exercise Creator

**Path:** `/scripts/cexc`

**Requirements:** None

A Bash script that creates a number of C code file template. It's really useful for exercises on schoolbooks.


### Ora

**Path:** `/scripts/ora.py`

**Requirements:** [gTTS (python)](https://pypi.org/project/gTTS/), [Playsound (python)](https://pypi.org/project/playsound/)

A stupid python script that says the time in italian. Sorry. 

I use this with cron (every 15 minutes). This script is inspired by the episode [6x17](https://www.imdb.com/title/tt3275762/) of the tv series "The Mentalist"


### MYP

**Path:** `/scripts/myp`

**Requirements:** ip, curl, hostname

Just a bash script that display the hostname and the local and public ip



## Crontab


### User Crontab

I put all my scripts in `/usr/local/bin/`, so:

```cron
00,15,30,45 * * * * /usr/local/bin/ora.py
00 12,17,21 * * * /usr/local/bin/backup
* * * * * /usr/local/bin/battery.sh
```

### Root Crontab

> I consider 'not-really-secure' everything that runs with root privileges. Be careful

```
00 19 * * * sudo dnf upgrade -y
```

Just updates the system every day at 19:00



## Dotfiles

In order to use i3 with plasma follow this guide https://github.com/heckelson/i3-and-kde-plasma


### i3pystatus

**Path (where the config file is in this repo):** `/dotfiles/i3pystatus/config.py`

**Config Path (where the file should be):** `~/.config/i3pystatus/config.py`

Layout (from left to right):

- Message (`Thinkpad T430 "ThiccPad"`)

- Audio Volume (`VOL: x%`)

- Battery Info (`BATT: DIS % AUTONOMY`)

- Temperature (`TEMP: x°C`)

- Date & Time (`DAY dd mmm HH:MM:SS PM/AM`)


### i3-gaps

**Path:** `/dotfiles/i3/config`

**Config Path:** `~/.config/i3/config`

Standard i3gaps config file with some additions:

- Compatibility with plasma as DE

- KRunner instead of DMenu

- `mod+shift+r` restarts i3 and set the wallpaper using feh

- Plasma logout screen

- i3pystatus as bottom bar

- set wallpaper at startup of i3

- 15 inner gaps


### VIM

**Path:** `/dotfiles/vimrc`

**Config Path:** `~/.vimrc`

I used to work with [VSCodium](https://vscodium.com/) (because yea, fuck microsoft) but vim has everything I need, and even more.

It's a pretty minimal vimrc. It has some autocompletation feature and a terminal window and NERDTree at startup


### ZSH

**Path:** `/dotfiles/zshrc`

**Config Path:** `~/.zshrc`

I use ZSH with the theme PowerLevel10k

There aren't special customization, just few plugins and aliases

#### Plugins

- git

- zsh-autocomplete

- zsh-interactive-cd

- history

#### Aliases

- `pip-req`=`pip install -r requirements.txt` 

- `please`=`sudo`  

- `r2p`=`cd ~/rasp2pc/ && python pc.py`  

- `sc`=`xrandr --output DP1 --auto --above LVDS1`

- `:q`=`exit`  

- `l`=`ls -l`  

- `la`=`ls -la`

- `packettracer`=`lookandfeeltool -a org.fedoraproject.fedora.desktop && /opt/pt/packettracer %f` 

- `dark`=`lookandfeeltool -a org.kde.breezedark.desktop`  

- `oclock`=`python3 ora.py`


