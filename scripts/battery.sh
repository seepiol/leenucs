#!/bin/bash

# battery.sh
# a simple bash script that checks if the laptop is on AC
# if unplugged, the tuned powersave profile is set. Else, balanced profile
# Set the execution of this script every minute with cron

ac_adapter=$(acpi -a | cut -d' ' -f3 | cut -d- -f1)

if [ "$ac_adapter" = "on" ]; then
    tuned-adm profile balanced
else
    tuned-adm profile powersave
fi
tuned-adm active
