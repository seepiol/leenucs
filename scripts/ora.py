#!/usr/bin/env python

# ORA.py
# Un semplice script python che dice l'ora ogni 15 minuti sfruttando la sintasi vocale di google
# Ispirato all'episodio 6x17 della serie tv "The Mentalist"

import gtts
from playsound import playsound
from datetime import datetime
from time import sleep

now = datetime.now()
ore = now.strftime("%H")
minuti = now.strftime("%M")

if (ore[0]=='0'):
    ore = ore[1:]

if (minuti=="00"):
    minuti = ""
elif (minuti=="15"):
    minuti = "e un quarto"
elif (minuti=="30"):
    minuti = "e mezza"
else:
    minuti = "e "+minuti

tts = gtts.gTTS(f"sono le {ore} {minuti}", lang="it")
tts.save("/home/sepi0l/ore.mp3")
playsound("/home/sepi0l/ore.mp3")
