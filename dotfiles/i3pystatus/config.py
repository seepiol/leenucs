from i3pystatus import Status

status = Status()

# Displays clock like this:
# Tue 30 Jul 11:59:46 PM KW31
#                          ^-- calendar week
status.register("clock",
    format="%a %-d %b %X",)

# Shows your CPU temperature, if you have a Intel CPU
status.register("temp",
    format="TEMP: {temp:.0f}°C",)

# The battery monitor has many formatting options, see README for details

# This would look like this, when discharging (or charging)
# ↓14.22W 56.15% [77.81%] 2h:41m
# And like this if full:
# =14.22W 100.0% [91.21%]
#
# This would also display a desktop notification (via D-Bus) if the percentage
# goes below 5 percent while discharging. The block will also color RED.
# If you don't have a desktop notification demon yet, take a look at dunst:
#   http://www.knopwob.org/dunst/
status.register("battery",
    format="BATT: {status}/{consumption:.2f}W {percentage:.2f}% {remaining:%E%hh:%Mm}",
    alert=True,
    alert_percentage=5,
    status={
        "DIS": "DIS↓",
        "CHR": "CHR↑",
        "FULL": "FULL=",
    },)
# Shows the address and up/down state of eth0. If it is up the address is shown in
# green (the default value of color_up) and the CIDR-address is shown
# (i.e. 10.10.10.42/24).
# If it's down just the interface name (eth0) will be displayed in red
# (defaults of format_down and color_down)
#
# Note: requires both netifaces and basiciw (for essid and quality)
#status.register("network",
#    interface="wlp3s0",
#    format_up="WLAN0: {essid} |  {v4}",)

#
# Note: requires libpulseaudio from PyPI
status.register("pulseaudio",
    format="VOL: {volume}%",)

status.register("text",
    text="Thinkpad T430 \"ThiccPad\""
    )

#status.register("window_title",
#    format="Window: {title}",
#    )

status.run()
